import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccorodianComponent } from './components/accorodian/accorodian.component';
import { ActionSheetComponent } from './components/action-sheet/action-sheet.component';
import { Tab1Page } from './tab1.page';

const routes: Routes = [
  {
    path: '',
    component: Tab1Page,
  },
  {
    path: 'action-sheet',
    component: ActionSheetComponent,
  },
  {
    path: 'ion-accorodian',
    component: AccorodianComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class Tab1PageRoutingModule {}
