import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { Camera, CameraResultType, CameraSource } from '@capacitor/camera';
import { Geolocation, Position } from '@capacitor/geolocation';
import { Share } from '@capacitor/share';

@Component({
  selector: 'app-tab1',
  templateUrl: 'tab1.page.html',
  styleUrls: ['tab1.page.scss'],
})
export class Tab1Page {
  myImage = null;
  position: Position = null;

  constructor(private router: Router) {}

  async takePicture() {
    const image = await Camera.getPhoto({
      quality: 90,
      allowEditing: true,
      resultType: CameraResultType.Uri,
      source: CameraSource.Camera,
    });
    this.myImage = image.webPath;
  }

  // Get Current Position
  async getCurrentPosition() {
    if (Geolocation.checkPermissions) {
      const coordinates = await Geolocation.getCurrentPosition();
      this.position = coordinates;
    }
  }

  async share() {
    await Share.share({
      title: 'Come and find me',
      text: `Here is my current location: ${this.position.coords.latitude} ${this.position.coords.longitude}`,
      url: 'http://ionicframework.com',
    });
  }

  openActionSheet() {
    this.router
      .navigateByUrl('tabs/tab1/action-sheet')
      .then((res) => console.log(res))
      .catch((e) => console.error(e));
  }

  openAccorodian() {
    this.router
      .navigateByUrl('tabs/tab1/ion-accorodian')
      .then((res) => console.log(res))
      .catch((e) => console.error(e));
  }
}
