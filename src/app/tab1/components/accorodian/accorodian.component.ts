import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController } from '@ionic/angular';

@Component({
  selector: 'app-accorodian',
  templateUrl: './accorodian.component.html',
  styleUrls: ['./accorodian.component.scss'],
})
export class AccorodianComponent implements OnInit {

  constructor(private navCtrl: NavController,
    private router: Router) { }

  ngOnInit() {}

  backToHome() {
    // this.navCtrl.pop();
    this.router.navigateByUrl('/tabs/tab1');
  }

}
